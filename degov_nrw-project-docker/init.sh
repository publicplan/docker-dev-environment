#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

__DIR__="$(cd "$(dirname "${0}")"; pwd)"

main() {
    if [ ! -d 'degov_nrw-project/' ]; then
        git clone git@bitbucket.org:publicplan/degov_nrw-project.git
    else
        echo "Folder exsists, not cloning again"
    fi
    #cd $__DIR__/degov_nrw-project
    #composer install
    echo "Running start.sh"
    bash start.sh
    echo "Running composer install -vvv"
    docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "composer install -vvv"
    echo "Pull lfs data"
    cd degov_nrw-project/docroot/profiles/contrib/nrwgov/testing/lfs_data
    git lfs pull
    cd $__DIR__
    echo "Create files/private folder"
    if [ ! -f 'degov_nrw-project/docroot/sites/default/files/private/' ]; then
    mkdir -p degov_nrw-project/docroot/sites/default/files/private/
    fi
    docker exec degov_nrw-project-docker_solr_1 solr create_core -p 8983 -c degov -d /own_config
    bash db_import.sh
    docker exec -it degov_nrw-project-docker_web_1 bash -c "chown -R apache:apache /var/www/app/docroot/sites/default/files/"
    echo "DONE!"
}

main
