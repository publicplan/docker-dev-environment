#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
if [[ -n "${DEBUG:-}" ]];then
 set -o xtrace
fi
# shellcheck disable=SC2164
readonly __DIR__="$(cd "$(dirname "${0}")"; pwd)"

main() {
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush sql-drop -y"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "rm -rvf docroot/sites/default/files/*"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "mkdir -pv docroot/sites/default/files/{private,translations}"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush sql-query -vvv --file=profiles/contrib/nrwgov/testing/lfs_data/nrwgov-7.x-dev.sql.gz"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "rm -vf docroot/profiles/contrib/nrwgov/testing/lfs_data/nrwgov-7.x-dev.sql"
  git -C degov_nrw-project/docroot/profiles/contrib/nrwgov/testing/lfs_data/ checkout nrwgov-7.x-dev.sql.gz
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y watchdog:delete all"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y cr"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y updb"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y pm:uninstall degov_demo_content"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y en degov_demo_content"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y cr"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y locale-check"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y locale-update"
  docker-compose -f docker-compose-cmd.yml run --rm cmd bash -c "drush -y cr"
}

main