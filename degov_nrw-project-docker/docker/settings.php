<?php
// @codingStandardsIgnoreFile
if (empty(getenv('APP_NAME'))) {
  die('Environment variable APP_NAME is not set.');
}

require_once __DIR__ . '/default.settings.php';

$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'database' => getenv('MYSQL_DATABASE')?: 'app',
      'username' => getenv('MYSQL_USERNAME')?: 'app',
      'password' => getenv('MYSQL_PASSWORD'),
      'host' => getenv('MYSQL_HOST')?: 'db',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

$drupal_hash_salt = hash('sha256', getenv('APP_NAME'));

drupal_fast_404();