<?php
$databases['default']['default'] = array(
  'database' => 'app',
  'username' => 'app',
  'password' => 'app',
  'prefix' => '',
  'host' => 'db',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$config['search_api.server.degov'] = [
  'backend_config' => [
    'connector_config' => [
      'host' => 'solr',
      'path' => '/',
      'core' => 'degov',
      'port' => '8983',
    ],
  ],
];
$settings['hash_salt'] = 'T592iAtyqwb-hS-0JrfvQuY0_f-wJkR7OS9ja0BFkDrGaHDut4K3ZBgpWmunqBuk7Jw1MMVLNw';
$config["system.file"]["path"]["temporary"] = "/tmp";
$settings["file_public_path"] = "sites/default/files";


/**
 * Enable local development services.
 */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$config['system.logging']['error_level'] = 'verbose';
