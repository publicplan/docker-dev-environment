# The docker-dev-environment for degov_nrw-project

- [ ] clone this repository 
  - ```git clone git@bitbucket.org:publicplan/docker-dev-environment.git```
- [ ] run [init.sh]()
  - ```./init.sh``` or ```bash init.sh```
- [ ] run [start.sh]()
  - ```./start.sh``` or ```bash start.sh```
- [ ] run [./db_import.sh]()
  - ```./start.sh``` or ```bash start.sh```
- [ ] add ```127.0.0.1 nrwgov.dd``` to your hosts file
  - Linux and MacOS located ```/etc/hosts```

Visit http://nrwgov.dd/ in your browser

## **Scripts explained**

- [init.sh]()
  - initializes the degov_nrw-project git and needed files/folders for docker
- [start.sh]()
  - this will start the docker enviroment
- [stop.sh]()
  - this will stop the docker enviroment
- [removeAll.sh]()
  - This will remove the docker enviroment
- [db_import.sh]()
  - This will import the needed test data for the project

---