#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

__DIR__="$(cd "$(dirname "${0}")"; pwd)"

main() {
  cd "$__DIR__" && \
  docker-compose --project-name="degov_nrw-project-docker" -f docker-compose-dev.yml down --volumes --remove-orphans --rmi local
  docker-compose --project-name="degov_project-docker-nginx-proxy" -f docker-compose-nginx.yml down --volumes --remove-orphans --rmi local
}

main
