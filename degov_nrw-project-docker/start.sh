#!/usr/bin/env bash
#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

__DIR__="$(cd "$(dirname "${0}")"; pwd)"

main() {
  docker network create proxy_front 2>/dev/null || true
  cp -r ./docker/settings.local.php ./degov_nrw-project/docroot/sites/default/
  docker-compose --project-name="degov_nrw-project-docker" -f docker-compose-dev.yml up -d --remove-orphans
  docker-compose --project-name="degov_project-docker-nginx-proxy" -f docker-compose-nginx.yml up -d --remove-orphans
}

main
