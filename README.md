# The docker-dev-environment

## **Install Docker**

**Linux** :
- [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
- [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
- [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
- [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Arch](https://wiki.archlinux.org/index.php/Docker)
- [Non listed Distro](https://docs.docker.com/install/linux/docker-ce/binaries/)
  
[Optinal steps after installation](https://docs.docker.com/install/linux/linux-postinstall/)

**MacOS**:
- [Docker Docs](https://docs.docker.com/docker-for-mac/install/)

**Windows**:
- [Docker Docs](https://docs.docker.com/docker-for-windows/install/)

----
### Install Docker-Compose

**Linux**:
- [Non listed Distro](https://docs.docker.com/compose/install/)
- [Arch AUR](https://www.archlinux.org/packages/community/any/docker-compose/)

**MacOS**:
- Included in the installer

**Windows**:
- Included in the installer
  
---

### **Checklist**

- [ ] Installed Docker?
- [ ] Checked console if I can access docker? ``` docker ps```
- [ ] Checked docker-compose? ```docker-compose --version```
